const defaultY = [
    0, 4, 11, 19, 26, 33, 47, 69, 98, 150, 205, 280, 360, 460, 555, 652, 737, 792, 837, 870, 882, 880,
    867, 835, 784, 720, 636, 520, 400, 310, 227, 165, 110, 78, 55, 40, 30, 25, 20, 10, 4, 2, 2, 2
];
let animationCallback = undefined;
let degrees = [];

for (let i = 0; i < 220; i += 5)
{
    degrees.push(i.toString());
}

console.log(degrees);
const acoptions = {
    type: 'line',
    data: {
        labels: degrees,
        datasets: [{
            label: 'New Tuning ',
            backgroundColor: "rgba(196, 93, 105, 0.3)",
            fill: true,
            data: defaultY.slice(),
            pointHitRadius: 50,
            pointHoverRadius: 10,
            borderWidth: 1,
            borderDash: [10, 10]
        }, {
            label: 'Old Tuning',
            backgroundColor: "rgba(32, 162, 219, 0.05)",
            fill: true,
            data: defaultY.slice(),
            borderWidth: 1
        }]
    },
    options: {
        responsive: false,
        tooltips: {
            enabled: false
        },
        animation: {
            duration: 1000,
            onComplete: function (animation) {
                if (typeof animationCallback === "function") {
                    // Execute custom follower/update complete callback function​
                    animationCallback();
                }
            }
        },

        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'RPM'
                }
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Power %'
                },
                ticks: {
                    reverse: false,
                    min: 0,
                    max: 1000
                }
            }]
        }
    }
};

const chartTune = document.getElementById('chartJSContainer');
const ctx = chartTune.getContext('2d');
const chartInstance = new Chart(ctx, acoptions);

d3.select(chartInstance.chart.canvas).call(
    d3.drag().container(chartInstance.chart.canvas)
        .on('start', getElement)
        .on('drag', updateData)
        .on('end', callback)
);

const par = {
    chart: undefined,
    element: undefined,
    scale: undefined,
    datasetIndex: undefined,
    index: undefined,
    value: undefined,
    grabOffsetY: undefined,
};


//Get an class of {points: [{x, y},], type: event.type} clicked or touched
function getEventPoints(event) {
    const retval = {
        point: [],
        type: event.type
    };
    //Get x,y of mouse point or touch event
    if (event.type.startsWith("touch")) {
        //Return x,y of one or more touches
        //Note 'changedTouches' has missing iterators and can not be iterated with forEach
        for (let i = 0; i < event.changedTouches.length; i++) {
            const touch = event.changedTouches.item(i);
            retval.point.push({
                x: touch.clientX,
                y: touch.clientY
            })
        }
    } else if (event.type.startsWith("mouse")) {
        //Return x,y of mouse event
        retval.point.push({
            x: event.layerX,
            y: event.layerY
        })
    }
    return retval;
}

function getElement() {
    const e = d3.event.sourceEvent;

    //would be nice if the reference to chart instance was found in e
    //How?

    par.scale = undefined;

    par.element = chartInstance.getElementAtEvent(e)[0];
    par.chart = par.element['_chart'];
    par.scale = par.element['_yScale'];

    par.datasetIndex = par.element['_datasetIndex'];
    par.index = par.element['_index'];

    //Get pixel y-offset from data point to mouse/touch point
    par.grabOffsetY = par.scale.getPixelForValue(
        par.chart.config.data.datasets[par.datasetIndex].data[par.index],
        par.index,
        par.datasetIndex,
        false
    ) - getEventPoints(e).point[0].y


}


function updateData() {
    const e = d3.event.sourceEvent;

    if (par.datasetIndex == 1) {
        return;
    }


    par.value = Math.floor(par.scale.getValueForPixel(
        par.grabOffsetY + getEventPoints(e).point[0].y) + 0.5);
    par.value = Math.max(0, Math.min(100, par.value));
    par.chart.config.data.datasets[par.datasetIndex].data[par.index] = par.value;
    chartInstance.update(0);
}


const chartXyDisplay = document.getElementById("yPos");

//Show y data after point drag
function callback() {
    chartXyDisplay.innerHTML = par.value;
}

//Apply changes to old data set
const history = [];
document.getElementById('applyChanges').addEventListener('click', function () {
    history.push(acoptions.data.datasets[1].data.slice());
    acoptions.data.datasets[1].data = acoptions.data.datasets[0].data.slice();
    chartInstance.update();
});

// Cancel changes - revert to old dataset
document.getElementById('cancelChanges').addEventListener('click', function () {
    acoptions.data.datasets[0].data = acoptions.data.datasets[1].data.slice();
    chartInstance.update();
});

// Cancel changes - revert to old dataset
document.getElementById('undoChanges').addEventListener('click', function () {
    const data = history.pop();
    if (data) {
        acoptions.data.datasets[0].data = data.slice();
        acoptions.data.datasets[1].data = data.slice();
        chartInstance.update();
    }
});
