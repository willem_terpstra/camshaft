class CubicSpline {


    static EQALXES = -2;  /* two x-coordinates are equal */
    static klo = -1;
    static khi = -1;

    /*
      spline constructs a cubic spline given a set of x and y values, through
      these values.

     */

    static spline(x, y, n, yp1, ypn, y2) {
        let u = [];
        if (yp1 > 0.99e30) {
            y2[0] = u[0] = 0.0;
        } else {
            y2[0] = -0.5;
            u[0] = (3.0 / (x[1] - x[0])) * ((y[1] - y[0]) / (x[1] - x[0]) - yp1);
        }
        for (let i = 1; i <= n - 2; i++) {
            let sig = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
            let p = sig * y2[i - 1] + 2.0;
            y2[i] = (sig - 1.0) / p;
            u[i] = (y[i + 1] - y[i]) / (x[i + 1] - x[i]) - (y[i] - y[i - 1]) / (x[i] - x[i - 1]);
            u[i] = (6.0 * u[i] / (x[i + 1] - x[i - 1]) - sig * u[i - 1]) / p;
        }
        let qn, un;
        if (ypn > 0.99e30) {
            qn = un = 0.0;
        } else {
            qn = 0.5;
            un = (3.0 / (x[n - 1] - x[n - 2])) * (ypn - (y[n - 1] - y[n - 2]) / (x[n - 1] - x[n - 2]));
        }
        y2[n - 1] = (un - qn * u[n - 2]) / (qn * y2[n - 2] + 1.0);
        for (let k = n - 2; k >= 0; k--) {
            y2[k] = y2[k] * y2[k + 1] + u[k];
        }
    }

    /*
      splint uses the cubic spline generated with spline to interpolate values
      in the XY  table.

     */

    static splint(xa, ya, y2a, n, x, y) {
        let r = 0;

        if (this.klo < 0) {
            this.klo = 0;

            this.khi = n - 1;
        } else {
            if (x < xa[this.klo]) {
                this.klo = 0;
            }
            if (x > xa[this.khi]) {
                this.khi = n - 1;
            }
        }
        while (this.khi - this.klo > 1) {
            let k = (this.khi + this.klo) >> 1;
            if (xa[k] > x) {
                this.khi = k;
            } else {
                this.klo = k;
            }
        }
        let h = xa[this.khi] - xa[this.klo];
        if (h == 0.0) {
            y = null;
            r = this.EQALXES;
        } else {
            let a = (xa[this.khi] - x) / h;
            let b = (x - xa[this.klo]) / h;
            y = a * ya[this.klo] + b * ya[this.khi] + ((a * a * a - a) * y2a[this.klo] +
                (b * b * b - b) * y2a[this.khi]) * (h * h) / 6.0;
        }
        return (r);
    }

    /*
    
      #>            spline1.dc2
    
      :     spline1
    
      Purpose:      1D cubic spline interpolation.
    
      Category:     MATH
    
      File:         spline.c
    
      Author:       P.R. Roelfsema
    
      Use:          INTEGER SPLINE1( XI   ,               Input   real( > NIN )
      YI   ,               Input   real( > NIN )
      NIN  ,               Input   integer
      XO   ,               Input   real( > NOUT )
      YI   ,               Output  real( > NOUT )
      NOUT ,               Output  integer
    
      SPLINE1 returns error codes:
      >0 - number of undefined values in YI.
      0 - no problems.
      -1 - not enough memory to create internal tables.
      -2 - the input array has two equal x-coordinates or
      value in XO outside range of input coordinates >
      interpolation problems.
      XI      Array containing input x-coordinates.
      YI      Array containing input y-coordinates.
      NIN     Number of (XI,YI) coordinate pairs.
      XO      Array containing x-coordinates for which y-coordinates
      are to be interpolated.
      YO      Array containing interpolated y-coordinates.
      If ALL YI are undefined, ALL YO are set to undefined.
      NOUT    Number of x-coordinates for which interpolation
      is wanted.
    
      Description:
    
      The interpolation is based on the cubic spline interpolation
      routines described in "Numerical recipes in C". For the interpolation
      data points in YI wich are undefined are skipped. Thus if undefined
      values are present, this routine will interpolate across them.
      XI data must be in increasing order. The number of undefined values
      in YI is returned.
    
      Updates:      Jan 29, 1991: PRR, Creation date
      Aug  7, 1991: MV,  Reset of 'klo' in 'spline1_c'
      Documentation about XI.
      Aug  9, 1993: PRR, Set YO to undefined when all YI undefined.
      #<
    
      Fortran to C interface:
    
      @ integer  spline1( real , real , integer , real , real , integer )
    
     */


    static spline1(xi, yi, nin, xo, yo, nout) {
        //#   float yp1 , ypn , *y2 , *xii , *yii , blank , alpha ;
        //#   fint  error , n , m , nblank , niin ;
//   setfblank_c( &blank ) ;
        let nblank = 0;
        let blank = null;
        for (let n = 0; n < nin; n++)
            if (yi[n] == blank)
                nblank++;

        let xii = [], yii = [];
        let niin = nin - nblank;
        if (nblank == 0) {
            xii = xi;
            yii = yi;
        } else if (nblank == nin) {
            for (let n = 0; n < nout; n++)
                yo[n] = blank;
            return (nblank);
        } else {
            xii = [];
            yii = [];
//      if ( !xii || !yii ) return( NOMEMORY ) ;
            for (let n = 0, m = 0; (n < nin) && (m < niin); n++) {
                if (yi[n] != blank) {
                    xii[m] = xi[n];
                    yii[m] = yi[n];
                    m += 1;
                }
            }
        }

        let y2 = [];

        let alpha = 1.0;
        let yp1 = 3e30 * alpha;
        let ypn = 3e30 * alpha;

        this.spline(xii, yii, niin, yp1, ypn, y2);

        this.klo = -1; /* Reset 'klo' before table interpolation */
        for (let n = 0; n < nout; n++) {
            this.splint(xii, yii, y2, niin, xo[n], yo[n]);
        }

        return (nblank);
    }

    interpolate(xIn, interval) {
        ksort(xIn, SORT_NUMERIC);
        xIn = array_filter(xIn, create_('i', 'return !empty(i);'));
        let xi = array_keys(xIn);
        let yi = array_values(xIn);

        let nin = count(xi);
        let start = xi[0];
        let end = xi[count(xi) - 1];


        let xo = [];
        for (let i = start; i <= end; i += interval) {
            xo.push(i);
        }
        let yo = [];
        let nout = count(xo);

        this.spline1(xi, yi, nin, xo, yo, nout);

        let rows = [];
        for (let i = 0; i < nout; i++) {
            rows.push({

                x: xo[i],
                y: yo[i]
            });
        }
        return rows;
    }

}

yo = [];

CubicSpline.spline1(
		[1000, 6000, 8000],
		[ 4, 60, 80],
		3,
		[ 2000, 3000, 4000, 5000, 7000],
		yo,
		5);
console.log(yo);

