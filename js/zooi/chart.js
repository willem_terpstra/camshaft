class Chart {

    cMap = null;

    constructor(id)
    {
        this.cMap = new CanvasMap(id);

        this.cMap.yOffset = 550;
        this.cMap.xOffset = 25;
    }

    yAxis()
    {
        this.cMap.strokeStyle("#000");
        this.cMap.fill();

        this.cMap.beginPath();
        this.cMap.moveTo(0,0);
        this.cMap.lineTo(0, this.cMap._heightMu);

        this.cMap.stroke();
    }

    xAxis()
    {
        this.cMap.strokeStyle("#000");
        this.cMap.fill();

        this.cMap.beginPath();
        this.cMap.moveTo(0,0);
        this.cMap.lineTo(this.cMap._widthMu, 0);

        this.cMap.stroke();
    }

    markers() {

    }


}