"use strict";
(function ($) {
    // You pass-in jQuery and then alias it with the $-sign
    // So your internal code doesn't change
})(jQuery);

$(document).ready(function () {
    // Get value on button click and show alert
    $("#redraw").click(function () {
        redraw(profileMeasured);
    });

    // Get value on button click and show alert
    $("#pause").click(function () {
        camshaft.bRunning = !camshaft.bRunning;
        $("#pause").html(camshaft.bRunning ? 'stop' : 'start');
    });

    // Get value on button click and show alert
    $("#step-mode").click(function () {
        camshaft.bStepMode = !camshaft.bStepMode;
        $("#step-mode").html(camshaft.bStepMode ? 'step mode' : '!step mode');
    });

    // Get value on button click and show alert
    $("#step").click(function () {
        camshaft.bStep = true;
        if (!camshaft.bStepMode) {
            camshaft.bStepMode = true;
            $("#step-mode").html('step mode');
        }
        if (!camshaft.bRunning) {
            camshaft.bRunning = true;
            $("#pause").html('stop');
        }
    });


    $("#wideness").change(function () {
            redraw(profileMeasured);
        }
    );

    $("#expMultiplier").change(function () {
            redraw(profileMeasured);
        }
    );

    $("#factor").change(function () {
            redraw(profileMeasured);
        }
    );

    $("#offset").change(function () {
            redraw(profileMeasured);
        }
    );

    $("#domain").change(function () {
            redraw(profileMeasured);
        }
    );


    $("#wideness0").change(function () {
            redraw(profileMeasured);
        }
    );

    $("#expMultiplier0").change(function () {
            redraw(profileMeasured);
        }
    );

    $("#factor0").change(function () {
            redraw(profileMeasured);
        }
    );
    $("#offset0").change(function () {
            redraw(profileMeasured);
        }
    );

    $("#domain0").change(function () {
            redraw(profileMeasured);
        }
    );

});


const _profileMeasured = [
    0, 20, 40, 110, 190, 260, 330, 470, 690, 980, 1500, 2050, 2800, 3600, 4600, 5550, 6520, 7370, 7920, 8370, 8700, 8820, 8800,
    8670, 8350, 7840, 7200, 6360, 5200, 4000, 3100, 2270, 1650, 1100, 780, 550, 400, 300, 250, 200, 100, 40, 20, 0
];
let profileMeasured = _profileMeasured;

const interpolateMethod = 'none';
// const interpolateMethod = 'lagrange';
// const interpolateMethod = 'cubicspline';

const interval = 10, intervalOffset = 5;
let zoom = 2;
let measurementIntervalDeg = 5; // Was 5, but interpolation doubles the number of points
if ('none' !== interpolateMethod) {
    // measurementIntervalDeg = 2.5;
    measurementIntervalDeg = 5 / (interval / intervalOffset);
}
const measurementIntervalRad = measurementIntervalDeg * (Math.PI / 180);
const startAngle = 2 * Math.PI + measurementIntervalRad;
let angle, tRad;
const camshaftDiameterMu = 22000;
const camshaftCircumRef = Math.PI * camshaftDiameterMu;
const camshaftRadius = camshaftDiameterMu / 2;
const intervalDistanceMu = (measurementIntervalDeg / 360) * camshaftCircumRef;


let profile;
const numSteps = (Math.PI * 2) / measurementIntervalRad;
let profileInfo;

// let followerRadius = 19050/2; // 3/4 inch radius
let followerRadius = 28575 / 2; // 1 1/8 inch radius
// let followerRadius = 5000; // debug
let evaluatedPoints = [];
let stepNum, verticalStep;

let cMap = new CanvasMap('canvas');

function deg2rad(deg) {
    return deg * (Math.PI / 180);
}

function rad2deg(rad) {
    return rad / (Math.PI / 180);
}

// Init

function initProfileInfo(profile, radAngle, measurementIntervalRad, intervalDistanceMu) {

    let profileInfo = [];

    profileInfo.push({
        aRad: 0,
        aDeg: 0,
        tRad: radAngle,
        h: 0,
    });

    radAngle += measurementIntervalRad;

    for (let i = 1; i <= numSteps; i++) {
        radAngle -= measurementIntervalRad;
        if ('undefined' !== typeof profile[i]) {
            const dt = profile[i] - profile[i - 1];
            const tan = dt / intervalDistanceMu;
            const aRad = Math.atan(tan);
            const aDeg = aRad * (180 / Math.PI);
            profileInfo.push({
                num: i,
                gaussNum: 'undefined' !== typeof profile[i].gaussNum ? profile[i].gaussNum : null,
                aDeg: aDeg,
                aRad: aRad,
                tRad: radAngle,
                h: 'undefined' !== typeof profile[i].y ? profile[i].y : profile[i],
            });
        } else {
            profileInfo.push({
                num: i,
                aDeg: 0,
                aRad: 0,
                tRad: radAngle,
                h: 0,
            });
        }
    }

    return profileInfo;
}


function interpolate(input, interval, intervalOffset, method = 'lagrange') {

    if ('none' === method) {
        return input;
    }

    // let interval = 10, intervalOffset = 5;
    let xOrg = [], xRes = [], xArr = [], intervalArr = [], res = null;

    for (let i = 0; i < input.length; i++) {
        xOrg[i * interval] = input[i];
        xArr[i * interval] = input[i];
        for (let j = intervalOffset; j <= interval; j += intervalOffset) {
            intervalArr.push(i * interval + j);
        }
    }

    switch (method) {
        case 'cubicspline':
            res = CubicInterpolation.doInterpolate(xArr, intervalArr);
            break;
    }

    let n;
    for (const [key, value] of Object.entries(xOrg)) {
        n = Number(key);
        xRes.push(value);
        xRes.push(res[n + intervalOffset]);
    }

    return xRes;
}

function getLabels(arr) {
    let ret = [];
    for (let i = 0; i < arr.length; i++) {
        ret.push(2 * i * measurementIntervalDeg);
    }
    return ret;
}

function ep(x, values, l) {
// console.log(numSteps);
// console.log(l);
    // let ePart0 = values.bulger.domain / l;
    // let x0 = x * ePart0;
    // x0 = x0 < 0 ? x0 * -1 : x0;

    let ePart = values.main.domain / l;

    let x1 = x * ePart;
    // x1 = x1 < 0 ? x1 * -1 : x1;
    x1 += Number(values.main.offset);

    // let y0 = values.bulger.factor * Math.exp(-Math.pow((x0) / values.bulger.wideness - values.bulger.offset, 2));
    // console.log ( 'e^(-(' + x0 + ' - ' + offset0 + ')^' + 2 );
    /**
     *

     */
    let denominator = 1 + x1;
    let divider = (0.6 * x1) / 3;
    // let y = values.main.factor * Math.exp(-Math.pow((x1 / values.main.wideness), values.main.expMultiplier));
    let y = Math.exp(-Math.pow(denominator / divider, values.main.expMultiplier));
    // console.log(x1);
    if ( x1 < -0.9 && x1 > -1.1 )
    {
        console.log(y);
    }
    // return y0 + y;
    return Number(values.main.factor) * y;
}

function derive(arr) {
    let ret = [];
    let diff = 0, slope = 0.0;
    for (let i = 0; i < arr.length; i++) {
        diff = arr[i + 1] - arr[i];
        slope = diff / intervalDistanceMu;
        ret.push(slope);
    }
    return ret;
}

let camshaft;

function main() {

    let numEvaluatedPoints = 0;
    let chart;
    evaluatedPoints = [];
    let completedSerie = [];
    let p = new Promise((resolve, reject) => {
        let it = setInterval(() => {
            // console.log('evaluatedPoints.length: ' + evaluatedPoints.length + ' profile.length: ' + profile.length);
            if (numEvaluatedPoints && numEvaluatedPoints === evaluatedPoints.length) { // Max reached
                completedSerie = evaluatedPoints;
                resolve();
                clearInterval(it);
            }
            numEvaluatedPoints = evaluatedPoints.length;

        }, 1000);

    }).then(function () {

        // completedSerie = profileMeasured;
        const labels = getLabels(completedSerie);
        const speed = derive(completedSerie);
        const acceleration = derive(speed);
        let ctx = document.getElementById("chart");
        let max = Math.round(Math.max(...completedSerie), 1000) * 1.1;


        chart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [
                    {
                        label: 'Lift',
                        fill: false,
                        borderColor: "orange",
                        backgroundColor: "orange",
                        pointBackgroundColor: "orange",
                        pointBorderColor: "#ccc",
                        pointHoverBackgroundColor: "#55bae7",
                        pointHoverBorderColor: "#55bae7",
                        pointRadius: 1,
                        yAxisID: 'A',
                        borderWidth: 1,
                        data: completedSerie
                    },
                    {
                        label: 'Speed',
                        fill: false,
                        borderColor: "blue",
                        backgroundColor: "blue",
                        pointBackgroundColor: "blue",
                        pointBorderColor: "#ccc",
                        pointHoverBackgroundColor: "#55bae7",
                        pointHoverBorderColor: "#55bae7",
                        yAxisID: 'B',
                        borderWidth: 1,
                        data: speed
                    },
                    {
                        label: 'Acceleration',
                        fill: false,
                        borderColor: "red",
                        backgroundColor: "red",
                        pointBackgroundColor: "red",
                        pointBorderColor: "#ccc",
                        pointHoverBackgroundColor: "#55bae7",
                        borderWidth: 1,
                        yAxisID: 'C',
                        data: acceleration
                    }
                ],
                labels: labels
            },
            options: {
                scales: {
                    yAxes: [{
                        id: 'A',
                        type: 'linear',
                        position: 'left',
                        ticks: {
                            max: max,
                            min: 0
                        }
                    }, {
                        id: 'B',
                        type: 'linear',
                        position: 'right',
                        ticks: {
                            min: -6,
                            max: 6
                        }
                    }, {
                        id: 'C',
                        type: 'linear',
                        position: 'right',
                        ticks: {
                            min: -0.001,
                            max: 0.001
                        }
                    }
                    ]
                }
            }
        })
    }.bind(this));
}

let interValFunc = null;

function init() {


    let run = function () {
        camshaft.run();
    }
    if (interValFunc) {
        clearInterval(interValFunc);
    }

    interValFunc = setInterval(function () {
        window.requestAnimationFrame(run);
        $("#step-counter").html(stepNum);
    }, 50);
}

function lerp(value1, value2, amount) {
    return (1 - amount) * value1 + amount * value2;
}


function collectInput() {
    // let bulger = {
    //     domain: $('#domain0').val(),
    //     expMultiplier: $('#expMultiplier0').val(),
    //     factor: $('#factor0').val(),
    //     offset: $('#offset0').val(),
    //     wideness: $('#wideness0').val()
    // };
    let main = {
        domain: $('#domain').val(),
        expMultiplier: $('#expMultiplier').val(),
        factor: $('#factor').val(),
        offset: $('#offset').val(),
        wideness: $('#wideness').val()
    };
    return {
        main: main,
        // bulger: bulger,
    };
}

function gauss(profile, values) {

    let domain = values.main.domain;
    let offset = values.main.offset;

    let es = [], ePart = domain / profile.length;
    let esX = 2000;
    if (offset) {
        esX += ePart * offset;
    }

    let y;
    for (let i = -profile.length / 2; i < profile.length / 2; i++) {
        y = ep(i, values, profile.length);
        es.push(
            {
                gaussNum: i,
                y: y
            });
    }

    camshaft.clearCanvas();
    camshaft.strokeStyle('black');
    camshaft.monitor(_profileMeasured);
    camshaft.strokeStyle('red');
    camshaft.monitor(es, esX, 6, 'y');
    return es;

}

function redraw(serie) {
    stepNum = 0;
    verticalStep = stepNum;
    zoom = $('#zoom').val();

    cMap.heightMu = 10000;
    cMap.widthMu = profileMeasured.length * intervalDistanceMu * zoom;
    cMap.wPx = 600;
    cMap.yOffset = 600;

    let values = collectInput();
    camshaft.setValues(collectInput());

    camshaft.clearCanvas();

    serie = gauss(serie, values);
    profileInfo = initProfileInfo(serie, startAngle, measurementIntervalRad, intervalDistanceMu);

    camshaft.setLength = serie.length;
    camshaft.setAngle(startAngle);
    camshaft.setProfile(profileInfo);
    camshaft.init();


    init();
    main();

}

// console.log($('#factor').val());


// let d1 = deriveSoft(profileMeasured);
// camshaft.monitor(d1, 2000,3000);
// let d2 = deriveSoft(d1);
// camshaft.monitor(d2, 2000, 30000000);

camshaft = new Camshaft();
redraw(profileMeasured);
