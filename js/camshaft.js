class Camshaft {

    console = null;
    bRunning = 0;
    bStepMode = false;
    bStep = false;
    follower = null;
    profile = null;
    angle = null;

    _setLength = 0;
    get setLength() {
        return this._setLength;
    }

    set setLength(value) {
        this._setLength = value;
    }

    constructor(profile) {
        this.console = new CanvasMap('console');
        this.follower = new Follower();
    }

    init() {
        this.console.heightMu = 10000;
        // this.console.widthMu = this.profile.length * intervalDistanceMu * zoom / 2;
        this.console.widthMu = zoom * 35000;
        this.console.wPx = 600;
        this.console.yOffset = 1200;
        this.console.xOffset = 0;
        // this.console.borderWidth = 2;
        this.console.strokeStyle = '#FF0000';
        // this.console.clearCanvas();
    }

    setValues(values) {
        this.follower.setValues(values);
    }

    setAngle(startAngle) {
        this.angle = startAngle;
    }

    setProfile(profile) {
        this.profile = profile;

    }

    calculateCircleCenter(A, B, C) {
        const yDelta_a = B.y - A.y;
        const xDelta_a = B.x - A.x;
        const yDelta_b = C.y - B.y;
        const xDelta_b = C.x - B.x;

        let center = [];

        const aSlope = yDelta_a / xDelta_a;
        const bSlope = yDelta_b / xDelta_b;

        center.x = (aSlope * bSlope * (A.y - C.y) + bSlope * (A.x + B.x) - aSlope * (B.x + C.x)) / (2 * (bSlope - aSlope));
        center.y = -1 * (center.x - (A.x + B.x) / 2) / aSlope + (A.y + B.y) / 2;
        return center;
    }

    clearCanvas() {
        this.console.clearCanvas();
    }

    strokeStyle(color) {
        this.console.ctx.strokeStyle = color;
    }

    monitor(points, xFactor = 2000, yFactor = 6, yProp = null) {
        // this.console.xOffset = xOffset;

        this.console.beginPath();
        let y = yProp ? points[0][yProp] : points[0];
        this.console.moveTo(0, points[0] * yFactor);
        for (let i = 1; i < points.length; i++) {

            // if (points[i].stepNum !== verticalStep) {
            //     continue;
            // }
            y = yProp ? points[i][yProp] : points[i];

            this.console.lineTo(i * xFactor, y * yFactor);

        }
        this.console.stroke();

        this.console.beginPath();
        for (let i = 1; i < points.length; i++) {

            // if (points[i].stepNum !== verticalStep) {
            //     continue;
            // }
            y = yProp ? points[i][yProp] : points[i];

            this.console.arc(i * xFactor, y * yFactor, 300);

        }
        this.console.stroke();
    }


    /**
     * From point x1, y1, calculate the endpoint for a line length r
     * and angle theta (in radials).
     * @param x1
     * @param y1
     * @param r
     * @param theta
     * @returns {{x: Number, y: Number}}
     */
    static calculateEndpoint(x1, y1, r, theta) {

        let x = x1 + r * Math.cos(theta);
        let y = y1 + r * Math.sin(theta);

        return {
            x: x,
            y: y
        };
    }

    run() {

        if (!this.bRunning) {
            return;
        }

        if (this.bStepMode) {
            if (!this.bStep) {
                return;
            }
            this.bStep = false;
        }

        let camEndPoints = [];

        cMap.clearCanvas();

        // Camshaft

        cMap.beginPath();
        cMap.moveTo(0, -4000);
        cMap.lineTo(0, 40000);
        cMap.stroke();

        // Shaft
        const ar = camshaftDiameterMu / 2;

        // Cam
        let aa = this.angle;
        let i;
        let ii = 0;
        let prev = null;
        let sinAa = null;
        verticalStep = 0;

        cMap.beginPath();
        let p, fza = 0, za;
        for (i = 0; i <= numSteps; i++) {

            p = Camshaft.calculateEndpoint(0, 0, ar + this.profile[i].h, aa);
            p.h = this.profile[i].h;
            p.gaussNum = this.profile[i].gaussNum;
            if (prev) {
                cMap.moveTo(prev.x, prev.y);
                cMap.lineTo(p.x, p.y);
            }

            sinAa = Math.sin(aa);

            if (1 === sinAa) {
                verticalStep = i;
            }

            if (Math.PI / 4 < sinAa) {
                cMap.moveTo(0, 0);
                cMap.lineTo(p.x, p.y);
                cMap.stroke();
                p.stepNum = this.profile[i].num;
                p.aa = aa;
                camEndPoints.push(p);
            }

            aa -= measurementIntervalRad;
            prev = p;

        }

        cMap.stroke();
        this.angle = this.profile[stepNum].tRad;
        // console.log('Camshaft angle: ' + 57.2957795 * this.angle);
        // console.log('first p.angle: ' + 57.2957795 * fza);
        // console.log('last p.angle: ' + 57.2957795 * za);

        this.follower.run(verticalStep, camEndPoints, this._setLength);
        this.monitor(evaluatedPoints);

        stepNum++;
        if (stepNum > numSteps - 1) {
            if ('#000000' === this.console.strokeStyle) {
                this.console.strokeStyle = "#FF0000";
            } else {
                this.console.strokeStyle = '#000000';
            }
            stepNum = 0;
        }
    }
}
