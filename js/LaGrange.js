class LaGrange {

    static interpolate(data, interpolateOffset, partSize) {

        let tmp = [];
        for (let i = 0; i < data.length; i++) {
            tmp.push({x: i, y: data[i]});
        }

        data = tmp;
        // let n = f.length;

        let result = 0; // Initialize result

        for (let i = 0; i < partSize; i++) {
            // Compute individual terms of above formula 
            let term = data[i].y;
            for (let j = 0; j < partSize; j++) {
                if (j != i)
                    term = term * (interpolateOffset - data[j].x) / (data[i].x - data[j].x);
            }

            // Add current term to result 
            result += term;
        }

        // console.log(result);
        return result;
    }
}