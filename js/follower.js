class Follower {

    // touchPoint = null;
    prevStepNum = null;
    prevTouchPoint = null;
    values = null;

    constructor() {
        this.prevTouchPoint = {h: 0};
    }

    setValues(values) {
        this.values = values;
    }

    showMessages(touchPoint, nextCamEndPoint) {
        $("#prev-val").text(' prev: ' + Math.round(this.prevTouchPoint.h.toString()));
        $("#current-val").text(' current: ' + Math.round(touchPoint.h).toString());
        let h1 = 0;
        if ('undefined' !== typeof nextCamEndPoint) {
            h1 = Math.round(this.yHeight(nextCamEndPoint.x, nextCamEndPoint.y)).toString();
        }
        $("#next-val").text(' next: ' + h1);
        $("#j-value").text(' selected step: ' + touchPoint.stepNum);
        let warning = '';
        if (this.prevStepNum === touchPoint.stepNum) {
            warning = ' warning: stepNum same as previous (' + this.prevStepNum + ')';
        }
        $("#warning").text(warning);

    }

    showPoint(pt, color, radius = 300) {

        cMap.beginPath();
        // cMap.ctx.globalAlpha = 1;
        cMap.fillStyle = color;
        cMap.moveTo(pt.x, pt.y);
        cMap.arc(pt.x, pt.y, radius);
        cMap.fill();
        cMap.stroke();
    }

    showPoints(tp, p1, p2) {
        this.showPoint(tp, 'black');
        this.showPoint(p1, 'red');
        this.showPoint(p2, 'blue');
    }

    createTouchPoint(endPoint, a, h) {
        const touchPoint = {
            // stepNum: endPoint.stepNum,
            // aa: endPoint.aa,
            // gn: endPoint.gn,
            // a: a,
            h: h,
            x: endPoint.x,
            y: endPoint.y,
        };
        return touchPoint;
    }

    /**
     * given an x and y point, calculate the height where the follower radius
     * will touch the center axis.
     * @param {Number} x
     * @param {Number} y
     * @returns {Number} Height on the center X-axis
     */
    yHeight(x, y) {
        let A = 0 - x;
        return y + Math.sqrt(Math.pow(followerRadius, 2) - Math.pow(A, 2));
    }

    cs(camEndPoints, setLength, interval) {

        let hh = 0;
        let touchPoint = {
            a: 0,
            h: 0
        };
        const ar = camshaftDiameterMu / 2;
        const m = 7;

        let l = camEndPoints.length;
        let interpolations = [        ];
        let hs = [];

        for (const [key, value] of Object.entries(camEndPoints)) {
            hs.push(value.h);
        }

        let firstH = Math.abs(hs[0]);
        let secH = Math.abs(hs[1]);
        // console.log(hs);
        for (let j = 0; j < 1; j += interval) {
            interpolations[j] = CubicInterpolation.doInterpolate(hs, j);
            for (let k = 0; k < interpolations[j].length; k++)
            {
                let ip = Math.abs(interpolations[j][k]);
                if (!(ip < firstH && ip > secH))
                {
                    interpolations[j].unshift();
                }
                else
                {
                    break;
                }
            }
            // console.log(interpolations[j]);
        }

        l = camEndPoints.length;
        for (let j = 0; j < l; j++) {
            let cp = camEndPoints[j], np = camEndPoints[j + 1];
            if ('undefined' === typeof cp || 'undefined' === typeof np) {
                // p1 = {x: 0, y: 0}, p2 = {x: 0, y: 0};
                continue;
            }

            this.showPoint(cp, 'white');
            for (let jj = 0; jj < 1; jj += interval) {

                // interpolate the angle using accumulated interval
                let gaa = lerp(cp.aa, np.aa, jj);

                // pp is current point on the cam
                let pp = Camshaft.calculateEndpoint(0, 0, ar + interpolations[jj][j], gaa);

                this.showPoint(pp, 'green');

                // yHeight is the center of the follower
                let h = this.yHeight(pp.x, pp.y);
                // console.log('stepNum: ' + camEndPoints[j].stepNum + ' h: ' + h);
                if (h > hh) {
                    this.showPoint(pp, 'orange');
                    // A higher touch point is found
                    touchPoint = this.createTouchPoint({
                        stepNum: camEndPoints[j].stepNum,
                        x: pp.x,
                        y: pp.y,
                    }, gaa, h);
                    hh = h;
                    // console.log('cs::hh: ' + hh);
                }
            }
        }
        return touchPoint;
    }

    ep(camEndPoints, setLength, interval) {

        let hh = 0;
        let touchPoint = {
            a: 0,
            h: 0
        };
        const l = camEndPoints.length;
        const ar = camshaftDiameterMu / 2;

        for (let j = 0; j < l; j++) {
            let cp = camEndPoints[j], np = camEndPoints[j + 1];

            if ('undefined' === typeof cp || 'undefined' === typeof np) {
                // p1 = {x: 0, y: 0}, p2 = {x: 0, y: 0};
                continue;
            }

            this.showPoint(cp, 'white');
            for (let jj = j; jj < j + 1; jj += interval) {
                if ('undefined' === typeof np) {
                    continue;
                }
                let ci = jj - j;
                let gn = lerp(cp.gaussNum, np.gaussNum, ci);

                // zh follows the actual curve (measuredPoints)
                let zh = ep(gn, this.values, setLength);

                // interpolate the angle using accumulated interval
                let gaa = lerp(cp.aa, np.aa, ci);

                // pp is current point on the cam
                let pp = Camshaft.calculateEndpoint(0, 0, ar + zh, gaa);

                // this.showPoint(pp, 'green');

                // yHeight is the center of the follower
                let h = this.yHeight(pp.x, pp.y);

                if (h > hh) {
                    // this.showPoint(pp, 'orange');
                    // A higher touch point is found
                    touchPoint = this.createTouchPoint({
                        stepNum: camEndPoints[j].stepNum,
                        x: pp.x,
                        y: pp.y,
                        aa: gaa,
                        gn: gn
                    }, gaa, h);
                    hh = h;
                    // console.log('ep::hh: ' + hh);
                }
            }
        }
        return touchPoint;
    }

    // Follower
    run(verticalStep, pCamEndPoints, setLength) {
        "use strict";
        // let a = measurementIntervalRad + Math.PI;
        cMap.beginPath();
        // a is the start angle on the follower (pointing left)
        // let a = measurementIntervalRad + Math.PI;

        // let hh = 0;
        let touchPoint = {
            a: 0,
            h: 0
        };

        let camEndPoints = [];

        let method = 'ep';
        /**
         * @type {number} number of points to measure from the centerpoint (left and right)
         */
        let m;
        if ('ep' === method) {
            m = 5;
        } else {
            m = 7;
        }

        let startStep = verticalStep - m, endStep = verticalStep + m;

        // let splineInput = [];

        for (const [key, value] of Object.entries(pCamEndPoints)) {
            if (value.stepNum < startStep) {
                continue;
            }
            if (value.stepNum > endStep) {
                break;
            }
            if ('undefined' === typeof value.gaussNum) {
                // a very high gaussNum will become near zero in function ep
                value.gaussNum = 9999;
            }
            camEndPoints.push(value);
            // let x = value.x.toString();
            // splineInput[x] = value.y;
        }
        let p1 = camEndPoints[0], p2 = camEndPoints[camEndPoints.length - 1];


        if ('cs' === method) {
            const interval = 1 / 128;
            touchPoint = this.cs(camEndPoints, setLength, interval);
            // console.log('**********************************************')
            // console.log(touchPoint);
            // console.log(this.ep(camEndPoints, setLength, interval));
        } else {
            const interval = 1 / 256;
            touchPoint = this.ep(camEndPoints, setLength, interval);
        }

        // Get the height of the center point of the follower in rest
        let y1 = camshaftRadius + followerRadius;
        if (touchPoint.h) {
            // console.log('l: ' + setLength + ' gn: ' + touchPoint.gn + ' zh: ' + zh);

            this.showPoints(touchPoint, p1, p2);

            // y1 is height of center point
            y1 = this.yHeight(touchPoint.x, touchPoint.y);

            // Draw the pendulum
            cMap.beginPath();
            cMap.fillStyle = "rgba(255, 255, 255, 0.5)"
            cMap.moveTo(touchPoint.x, touchPoint.y);
            cMap.lineTo(0, y1);
            cMap.stroke();

            if (touchPoint.y - camshaftRadius && evaluatedPoints.length < profileMeasured.length) {
                evaluatedPoints.push(touchPoint.y - camshaftRadius);
            }
            this.prevStepNum = touchPoint.stepNum;
        }

        // Draw the follower
        cMap.beginPath();
        cMap.fillStyle = "rgba(255, 255, 255, 0.5)"
        // cMap.fillStyle = 'orange';
        cMap.arc(0, y1, followerRadius);
        cMap.stroke();
        cMap.fill();
    }

}
