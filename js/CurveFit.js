/*
        y1 (x-x2) (x-x3) (x-x4)
f(x) = ---------------------------
         (x1-x2) (x1-x3) (x1-x4)

        y2 (x-x1) (x-x3) (x-x4)
     + ---------------------------
         (x2-x1) (x2-x3) (x2-x4)

        y3 (x-x1) (x-x2) (x-x4)
     + ---------------------------
         (x3-x1) (x3-x2) (x3-x4)

        y4 (x-x1) (x-x2) (x-x3)
     + ---------------------------
         (x4-x1) (x4-x2) (x4-x3)
 */

/**
 *
 */

function f(points, x) {
    let r1 = 0;
    for (let i = 0; i < points.length; i++) {

        let y = points[i].y, x1 = points[i].x;

        if ('undefined' === typeof points[i + 3]) {
            break;
        }

        let sp = '', sq = '';
        let o = i % 4;
        let p = [], q = [];
        for (let j = 0; j < 4; j++) {
            if (x === points[i + j].x) {
                p[j] = 1;
            } else {
                p[j] = x - points[i + j].x;
                sp += '(x - x' + (i + j) + ') ';
            }

            if (x1 === points[i + j].x) {
                q[j] = 1;
            } else {
                q[j] = x1 - points[i + j].x;
                sq += '(x' + i + ' - x' + (i + j) + ')';
            }
        }


        let r = (y * p[0] * p[1] * p[2] * p[3])
            / (q[0] * q[1] * q[2] * q[3]);
        console.log('y' + i + ' * ' + sp + ' / ' + sq + ' => ' + r);
        r1 += r;
    }
    return r1;
}

let p = [
    {x: 1, y: 2},
    {x: 2, y: 2.1},
    {x: 3, y: 3},
    {x: 4, y: 3.5},
    {x: 5, y: 4.5},
    {x: 6, y: 3.5},
    {x: 7, y: 5.5},
    {x: 8, y: 3.5},
    // {x: 9, y: 2.5},
    // {x: 10, y: 3.5},
    // {x: 11, y: 5.5},
    // {x: 12, y: 4.5},
    // {x: 13, y: 5.5},
    // {x: 14, y: 6.5},
    // {x: 15, y: 7.0},
    // {x: 16, y: 7.5},
];

for (let i = 0; i < p.length; i += 0.1) {
    console.log(f(p, i));
}

