/**
 * @class CanvasMap
 */
class CanvasMap {


    canvas = null;
    ctx = null;
    yOffset = 0;
    xOffset = 500;

    constructor(id) {
        if (this.ctx) {
            throw "ctx is not static";
        }
        // console.log('creating ctx for: ' + id);

        this.canvas = document.getElementById(id);
        // console.log(this.canvas);
        if (this.canvas.getContext) {
            this.ctx = this.canvas.getContext('2d');
            this._font = this.ctx.font;
            // this.ctx.borderWidth = 4;
            // this.ctx.lineWidth = 2;
            // this.ctx.globalAlpha = 0.5
            // console.log(this.ctx);
        }
    }

    _fillStyle = '';

    get fillStyle() {
        return this._fillStyle;
    }

    set fillStyle(value) {
        this._fillStyle = value;
        this.ctx.fillStyle = value;
    }


    get strokeStyle() {
        return this.ctx.strokeStyle;
    }

    set strokeStyle(value) {
        this.ctx.strokeStyle = value;
    }

    _font = '';
    get font() {
        return this._font;
    }

    set font(value) {
        this._font = value;
        this.ctx.font = value;
    }

    _heightMu = 0;
    set heightMu(value) {
        this._heightMu = value;
    }

    _lineWidth = 1;
    get lineWidth() {
        return this._lineWidth;
    }

    set lineWidth(value) {
        this._lineWidth = value;
        this.ctx.lineWidth = value;
    }

    _widthMu = 0;
    set widthMu(value) {
        this._widthMu = value;
    }

    // get widthMu() {
    //     return this._widthMu;
    // }

    _wPx = 0;
    set wPx(value) {
        this._wPx = value;
        // screen ratio for correct display of angles
        this.hPx = (this._heightMu / this._widthMu) * this._wPx;
        this.xStep = this._wPx / this._widthMu;
        this.yStep = this.hPx / this._heightMu;
    }

    hPx = 0;
    xStep = 0;
    yStep = 0;

    xCoord(x) {
        return x * this.xStep;
    }

    // centerX() {
    //     console.log(this.xCoord(this._widthMu/2));
    //     return this.xCoord(this._widthMu/2);
    // }

    yCoord(y) {
        return this.yOffset - this.yStep * y;
    }

    clearCanvas() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    arc(ax, ay, ar, start = 0, end = Math.PI * 2,b = true, debug = false) {
        ax = this.xCoord(ax) + this.xOffset;
        ay = this.yCoord(ay);
        ar = this.xCoord(ar);

        if (debug) {
            console.log('arc x: ' + ax + ' y: ' + ay + ' r: ' + ar);
        }

        this.ctx.arc(ax, ay, ar, start, end, b);
    }

    beginPath() {
        this.ctx.beginPath();
    }

    // fillStyle(color) {
    //     this.ctx._fillStyle = color;
    // }

    fill() {
        this.ctx.fill();
    }

    fillText(text, x, y) {
        x = this.xCoord(x) + this.xOffset;
        y = this.yCoord(y);

        this.ctx.fillText(text, x, y);
    }

    lineTo(x, y, debug = false) {
        if (debug) {
            console.log('lineTo x-in: ' + x + ' y-in: ' + y);
        }
        x = this.xCoord(x) + this.xOffset;
        y = this.yCoord(y);

        if (debug) {
            console.log('lineTo x: ' + x + ' y: ' + y);
        }

        this.ctx.lineTo(x, y);
    }

    moveTo(x, y, debug = false) {
        x = this.xCoord(x) + this.xOffset;
        y = this.yCoord(y);

        if (debug) {
            console.log('moveTo x: ' + x + ' y: ' + y);
        }
        this.ctx.moveTo(x, y);
    }

    stroke() {
        this.ctx.stroke();
    }

    strokeStyle(color) {
        this.ctx.strokeStyle = color;
    }
}