class CubicInterpolation {

    static oldY;
    static delta = 0;

    static y(y0, y1, y2, y3, mu, tension = 0, bias = 0) {
        let m0, m1;
        let a0, a1, a2, a3, mu2, mu3;

        mu2 = mu * mu;
        mu3 = mu2 * mu;
        m0 = (y1 - y0) * (1 + bias) * (1 - tension) / 2;
        m0 += (y2 - y1) * (1 - bias) * (1 - tension) / 2;
        m1 = (y2 - y1) * (1 + bias) * (1 - tension) / 2;
        m1 += (y3 - y2) * (1 - bias) * (1 - tension) / 2;

        a0 = 2 * mu3 - 3 * mu2 + 1;
        a1 = mu3 - 2 * mu2 + mu;
        a2 = mu3 - mu2;
        a3 = -2 * mu3 + 3 * mu2;

        return (a0 * y1 + a1 * m0 + a2 * m1 + a3 * y2);

    }

    static doInterpolate(inData, x) {
        let rows = [];

        // Add 1 item with same slope as the first
        // at the begin of the array
        let startDiff = inData[1] - inData[0];
        inData.unshift(inData[0] - startDiff);

        // Add 3 items with the same slope as the last
        // to the end of the array
        let l = inData.length;
        let endDiff = inData[l-1] - inData[l-2];
        inData.push(inData[l-1]+endDiff);
        inData.push(inData[l]+endDiff);
        inData.push(inData[l]+endDiff);

        for (let i = 0; i < inData.length - 4; i++) {
            // let i = 0;
            let y0 = inData[i],
                y1 = inData[i + 1],
                y2 = inData[i + 2],
                y3 = inData[i + 3];

            let r = CubicInterpolation.y(y0, y1, y2, y3, x);
            rows.push(r);
        }

        // Remove added data
        inData.shift();
        inData.pop();
        inData.pop();
        inData.pop();
        return rows;
    }

}

// Test with sinus curve
// let x = 0.5;
// let a = [], b = [];
// for (let d = 0; d < Math.PI * 2; d += (Math.PI) / 20) {
//     a.push(10 * Math.sin(d));
// }
// for (let i = 0, d = 0; d < Math.PI * 2; d += (Math.PI) / 40, i++) {
//     if (i % 2) {
//         b.push(10 * Math.sin(d));
//     }
// }
//
// let rows = CubicInterpolation.doInterpolate(a, x);
// console.log(rows);
// console.log(b);
// }
